# EDCL Considerations for Adoption

This document will cover some of the practical or technical aspects that need
to be considered when adopting the [EDCL] \(European Digital Credentials for
Learning) or [EDCI] \(Europass Digital Credentials Infrastructure)
arhitecture. The views presented here are very much based on the experience of
working with local installations of the EDCI software, and attempts at
interfacing with it. Documentation on this can be found in the (unofficial)
[EDCI Documentation] repository.

[EDCL]: https://europa.eu/europass/en/european-digital-credentials-learning
[EDCI]: https://europa.eu/europass/what-are-digital-credentials
[EDCI Documentation]: https://gitlab.com/tau-novi-research-group/edci-documentation

## The General Structure of the EDCI architecture

The [EDCI] consists of 3 major pre-existing applications: the *EDCI Viewer*,
*EDCI Wallet* and *EDCI Issuer*. The way in which these 3 applications
interface with each other is presented in Figure 1.

![Figure 1: EDCI architecture](./images/edci-architecture.svg "Figure 1: EDCI architecture")

**Figure 1:** An illustration of the EDCI architecture.

The major thing to note is that all of the 3 applications need to be connected
to an [*OpenID Connect*- or OIDC][OIDC]-based authentication server. One
alternative server application that can be used for this purpose is
[*Keycloak*][Keycloak], as it is mature enough to be used in production, if
needed. As a user tries to log in to one of the applications, they need to be
redirected to the login page provided by the active OIDC provider instance,
and if a login is successful, the user will be given a personal access token,
which allows them to access the 3 applications for as long as the token is
valid. This is already implemented in the 3 EDCI applications, but this also
needs to be considered, if one decides to write their own [RESTful] client application
that interfaces with the EDCI architecture.

[OIDC]: https://openid.net/connect/
[Keycloak]: https://www.keycloak.org/
[RESTful]: https://en.wikipedia.org/wiki/Representational_state_transfer

## The roles of the 3 pre-existing EDCI applications…

… are as follows: the [EDCI Wallet] is responsible for storing
Europass-compliant digital credentials, the [EDCI Issuer] can be used to issue
them (directly to a user Wallet or via e-mail) and the [EDCI Viewer], as its
name implies, can be used to view digital credentials issued by the EDCI
Issuer. Here **Europass-compliancy** means that the digital credentials should
be stuctured in the way defined by the [Europass Learning Model], but also
stored in a correct format. The EDCI applications as they are now support
digital credential transmission via [JSON], but the credentials themselves are
stored as [XML] strings within the JSON objects, as they are transmitted over
the wire.

[EDCI Issuer]: https://europa.eu/europass/en/european-digital-credentials-learning-interoperability#3112
[EDCI Wallet]: https://europa.eu/europass/en/european-digital-credentials-learning-interoperability#3112
[EDCI Viewer]: https://europa.eu/europass/en/european-digital-credentials-learning-interoperability#3112

[Europass Learning Model]: https://github.com/european-commission-europass/Europass-Learning-Model
[JSON]: https://www.json.org/
[XML]: https://www.w3.org/XML/

Should an institution wish to *issue* digital credentials, the [EDCI Issuer]
is the application to use. An educational institution or any other type of
user can install a local version of the Issuer, enter their course information
into its database via the Issuer UI, and then start issuing digital
credentials, which work as proofs of completion of the activities that the
user has entered into the Issuer database. Note that a valid and an official
user-specific **Digital Seal** in the form of a USB stick needs to be attached
to a computer for the issuing via the UI or the [API][issuer-api] to succeed.
The reason for this restriction is that digital credentials are supposed to be
unique and identifiable proofs of merit, and without such a seal this would
not be the case.

[issuer-api]: https://gitlab.com/tau-novi-research-group/edci-documentation/-/blob/main/apis/issuer_api.json

The [EDCI Wallet] is the application that handles the actual storage of the
digital credentials (among these 3 applications) in its database, should a
user decide to establish a [Europass portfolio], which functions as a
front-end to the Wallet. Similarly to the Europass site, an application
wishing to view said credentials needs to communicate with the EDCI Wallet via
its [API][wallet-api], which (version 1.4.1) is again documented in the
[unofficial EDCI documentation][EDCI documentation]. Once a credential has
been issued via the EDCI Issuer, an e-mail is sent to the person to whom the
credential has been issued and if a Wallet exists, the credential is also
stored there.

[wallet-api]: https://gitlab.com/tau-novi-research-group/edci-documentation/-/blob/main/apis/wallet_api.json
[Europass portfolio]: https://europa.eu/europass/en

The sole purpose of the [EDCI Viewer] is to function as a user-available
method of viewing, verifying (by communicating with the Wallet) and possibly
converting to other formats (single) digital credentials received from the
EDCI Issuer. Especially because it is limited to viewing a single credential
at a time, the Viewer then holds less importance than the other two
applications, but it could be used as an inspiration or as a starting point
for constructing a more robust application for viewing aggregate views of data
related to Europass credentials.


## On Europass-compliancy

As was mentioned above, any application or organisation wishing to interface
with the EDCI architecture needs to be able to process Europass-compliant
credentials, which implies being able to parse *and* produce [Europass
Learning Model] -conforming credentials. For example, an educational
institution might have their own data model and -structure for storing digital
credential -related data, which again might differ from the structure imposed
by the [Europass Learning Model]. To store credentials in the [EDCI Wallet] from
the database of the institution, the institution would then need to convert
their own data to a fromat that the EDCI applications understand.

There are a few things that need to be considered here, and how the issue of
differing data models or schema might be tackled. **First**, any institutions
wishing to partake in issuing or storing Europass-compliant credentials might
transform their own data model to reflect the stucture presented in the
[Europass Learning Model] better. This might include adding fields such as
international learner identifiers and the like, which would map directly from
and to the Europass Learning Model and the institutions own data schema. **A
second** measure to take would be to actually establish the mapping between
the different schema. This would mean writing a small compiler, which would
read institution- or user-specific data formats as its input and produce as
its output a Europass credential:

	Institution-specific digital credential ↦ Europass Credential .

One tool to approach this with might be the (very handy) [Serde] serialization
and deserialization library for the [Rust] programming language, which could
be used to produce the compiler binary, which could then be placed in a
pipeline of one's choosing. It would be best if this project was publicly
available, and written in collaboration with the institutions that wish to use
it. The beginnings of such a compiler can be found in the [↦ Europass][Map to
Europass] repository. Somebody simply needs to write it.

**Note:** This need for a compiler somewhere along the digital credential
transmission pipeline does not change, if a blockchain technology such as the
one provided by [EBSI] (European Blockchain Services Infrastructure) instead
of the [EDCI Wallet] were use to store the credentials, unless institutions
decided to adopt the [Europass Learning Model] in full. The [Europass Learning
Model] as a common data model for storing digital credentials is the one ring
to rule them all, and the technology should be built to support it for the
digital credentials infrastructure to function seamlessly.

[Serde]: https://serde.rs/
[Rust]: https://www.rust-lang.org/
[Map to Europass]: https://gitlab.com/tau-novi-research-group/map-to-europass
[EBSI]: https://ec.europa.eu/digital-building-blocks/wikis/display/CEFDIGITAL/EBSI


## On storing digital credentials

As was mentioned before, the [EDCI] applications contain among them the [EDCI
Wallet], which is a "simple" [relational database] -based solution for storing
digital credentials, for the people who choose to sign up with [Europass]. The
[EDCI Issuer] is also capable of sending issued credentials to the credential
owner via e-mail, which is its default mode of operation, even if no
[Europass] account has been set up by the recipient.

[relational database]: https://en.wikipedia.org/wiki/Relational_database
[Europass]: https://europa.eu/europass/en

The purpose behind sending the digital credential as an e-mail attachment
regardless of whether the recipient's [Europass] account exists is to *give
natural persons ownership over their own digital data*, in addition to
complying with the European [GDPR] (General Data Protection Regulation)
legislation. The data ownership should be achievable even if no central
database solution for storing the digital credentials exists, and hence an
easy-to-implement solution is to simply pass the digital credentials directly
to their owners.

[GDPR]: https://europa.eu/youreurope/business/dealing-with-customers/data-protection/data-protection-gdpr/index_en.htm

A simple [relational database] can be problematic from a *data ownership
perspective*, as it is not a naturally [distributed system] nor a
[decentralized one][decentralized system]: the contents of a relational
database are stored in a single location (database files on a single server),
and even though backups and copies may be created of the database (as it is a
simple set of files), synchronising these between locations requires building
a separate automation framework to make it automatic. There is also the
question of *whose relational database should be the canonical one*, if copies
of the database were distributed among many entities. These things among
others make a relational database an un-ideal solution for storing digital
credentials.

[distributed system]: https://en.wikipedia.org/wiki/Distributed_computing
[decentralized system]: https://en.wikipedia.org/wiki/Decentralized_application

An example of a [decentralized system] that might work a little better with
the idea of storing personal and digital data in a manner that allows natural
persons to hold ownership over said data is a [blockchain], such as the
previously mentioned [EBSI]. Figure 2 shows an example of how a blockchain
might be structured.

![Figure 2: Structure of the BitCoin blockchain. (Wikimedia Commons)](./images/bitcoin-blockchain-structure.svg "Figure 2: Structure of the BitCoin blockchain. (Wikimedia Commons)")

**Figure 2:** A single copy of *the* BitCoin database. Copies of this would be
automatically distributed among the parties participating in the blockchain.
(Wikimedia Commons)

A blockchain is still a database, with each `Tx_Root` in Figure 2 holding one
set of data, but the distribution of the data with a high amount of security
among multiple entities is built into the system: *automatic* communication
between blockchain nodes (copies of the database on different computers) is
based on [peer-to-peer] technology, and entries already in the system can no
longer be modified with the means provided by the blockchain system itself, as
new blocks of data depend on previously added data via a cryptographic hash
`Prev_Hash` and a ledger that keeps track of the data.

[blockchain]: https://en.wikipedia.org/wiki/Blockchain
[peer-to-peer]: https://en.wikipedia.org/wiki/Peer-to-peer

If there is to be a commonly agreed-upon storage location for digital
credentials that adhere to the [Europass Learning Model], then a blockchain
such as [EBSI] should definitely be considered as a prime alternative to a
relational database. It gets rid of the *problem of canonicity* (whose
database is to be the [single source of truth]), as everybody connected to the
blockchain automatically get the same copy of the data. Blockchains are also
more resilient to modification attempts, and a summer trainee working at one
of the blockchain nodes cannot accidentally erase entries in the database. The
users of the blockchain simply need to be provided access by a wallet
front-end.

[single source of truth]: https://en.wikipedia.org/wiki/Single_source_of_truth

## Conclusion

At this point it should be obvious that the single most important aspect of
providing learners and professionals ownership over their digital selves is to
have a common standardized data model, an example of which is the [Europass
Learning Model]. Were institutions and companies to adopt this, transferring
digital credentials between them would become effortless. Until then some kind
of a translation to a common data model needs to be provided, which in
practise boils down to writing a compiler or a set of compilers that are able
to transfer digital credentials from one form to another.

This is especially true if a common storage framework for the credentials is
to be set up. Any data stored in a system should have a pre-defined structure,
to make the storage practical. Otherwise the only option is to store the data
as binary "blobs", which is not practical from an information retrieval
perspective.

A traditional relational database with no extra functionality is not a
solution that answers the issues of data ownsership in a common storage
framework, even if the were set up in a distributed manner, such as within a
[Distributed Relational Database Architecture][DRDA] ([DRDA]), as simply
distributing the relational database does not take care of the
decentralization aspect or the problem of canonicity: whose copy of the
database is to be the [single source of truth] in the system? More
sophisticated solutions are then needed. One alternative is to set up a
[blockchain] such as [EBSI], which would take care of automatic
decentralization and protect its users against unintentional and possibly even
intentional data corruption.

[DRDA]: https://en.wikipedia.org/wiki/DRDA
